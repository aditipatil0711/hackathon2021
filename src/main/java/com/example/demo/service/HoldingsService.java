package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Holdings;
import com.example.demo.repository.HoldingsRepository;
@Service
public class HoldingsService {
	@Autowired
	private HoldingsRepository repository;

	public List<Holdings> getAllHoldings() {
		return repository.getAllHoldings();
	}

	public Holdings getHoldingsByName(String stockTicker) {
		return repository.getHoldingsByName(stockTicker);
	}

	public Holdings saveHoldings(Holdings holdings) {
		return repository.editHoldings(holdings);
	}

	public Holdings newHoldings(Holdings holdings) {
		return repository.addHoldings(holdings);
	}

	public String deleteHoldingsByName(String stockTicker) {
		return repository.deleteHoldingsByName(stockTicker);
	}

	
	}

