package com.example.demo.entities;

public class Holdings {
	
	
	private String stockTicker;
	private int volume;
	private double buying_price;

	public Holdings() {
		
	}

	public Holdings( String stockTicker, int volume,double buying_price) {
		
		this.stockTicker = stockTicker;
		this.volume = volume;
		this.buying_price=buying_price;
	}

	public double getBuying_price() {
		return buying_price;
	}

	public void setBuying_price(double buying_price) {
		this.buying_price = buying_price;
	}

	
	public void setVolume(int volume) {
		this.volume = volume;
	}
	
	public int getVolume() {
		return volume;
	}

	public String getStockTicker() {
		return stockTicker;
	}

	public void setStockTicker(String stockTicker) {
		this.stockTicker = stockTicker;
	}
	
	
}
