package com.example.demo.repository;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.entities.Holdings;
@Component
public interface HoldingsRepository {
	public List<Holdings> getAllHoldings();

	public Holdings getHoldingsByName(String stockTicker);
	
	public Holdings editHoldings(Holdings holdings);

	public String deleteHoldingsByName(String stockTicker);

	public Holdings addHoldings(Holdings holdings);

	
}
