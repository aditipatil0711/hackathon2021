package com.example.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Holdings;

@Repository
public class MYSQLHoldings implements HoldingsRepository {
	@Autowired
	JdbcTemplate template;
	@Override
	public List<Holdings> getAllHoldings() {
		// TODO Auto-generated method stub
		String sql = "SELECT stockTicker, volume, buying_price FROM holdings";
		return template.query(sql, new HoldingsRowMapper());
	}

	@Override
	public Holdings getHoldingsByName(String stockTicker) {
		String sql = "SELECT stockTicker, volume,buying_price FROM holdings WHERE stockTicker=?";
		return template.queryForObject(sql, new HoldingsRowMapper(), stockTicker);
	}

	@Override
	public Holdings editHoldings(Holdings holdings) {
		String sql = "UPDATE holdings SET stockTicker = ?, volume = ?, buying_price = ?"
				+ "WHERE stockTicker = ?";
		template.update(sql, holdings.getStockTicker(), holdings.getVolume(), holdings.getBuying_price());
		return holdings;
	}

	@Override
	public String deleteHoldingsByName(String stockTicker) {
		String sql = "DELETE FROM holdings WHERE stockTicker = ?";
		template.update(sql, stockTicker);
		System.out.println(stockTicker);
		return stockTicker;
	}

	@Override
	public Holdings addHoldings(Holdings holdings) {
		String sql = "INSERT INTO holdings( stockTicker, volume , buying_price) " + "VALUES(?,?,?)";
		template.update(sql, holdings.getStockTicker(), holdings.getVolume(),holdings.getBuying_price());
		System.out.println(holdings);
		return holdings;
		
	}
	
	class HoldingsRowMapper implements RowMapper<Holdings> {

		@Override
		public Holdings mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Holdings( rs.getString("stockTicker"),  rs.getInt("volume"), rs.getDouble("buying_price"));
		}

	}

}
