package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Holdings;
import com.example.demo.entities.Stock;
import com.example.demo.service.HoldingsService;
import com.example.demo.service.StockService;

@CrossOrigin
@RestController
@RequestMapping("api/stocks")
public class StockController {

	@Autowired
	StockService service;
	
	
	@Autowired
	HoldingsService service2;

	@GetMapping(value = "/")
	public List<Stock> getAllStocks() {
		return service.getAllStocks();
	}

	@GetMapping(value = "/{id}")
	public Stock getStock(@PathVariable("id") int id) {
		return service.getStock(id);

	}

	@PostMapping(value = "/")
	public Stock addStock(@RequestBody Stock stock) {
		return service.newStock(stock);
	}

	@PutMapping(value = "/")
	public Stock editStock(@RequestBody Stock stock) {
		return service.saveStock(stock);
	}

	@DeleteMapping(value = "/{id}")
	public int deleteStock(@PathVariable int id) {
		return service.deleteStock(id);
	}

	@GetMapping(value = "/buy")
	public List<Stock> getBoughtStocks() {
		return service.getBoughtStocks();
	}
	
	

	@GetMapping(value = "/holdings/")
	public List<Holdings> getAllHoldings() {
		return service2.getAllHoldings();
	}

	@GetMapping(value = "/holdings/{stockTicker}")
	public Holdings getHoldings(@PathVariable("stockTicker") String stockTicker) {
		return service2.getHoldingsByName(stockTicker);

	}

	@PostMapping(value = "/holdings/")
	public Holdings addHoldings(@RequestBody Holdings holdings) {
		return service2.newHoldings(holdings);
	}

	@PutMapping(value = "/holdings/")
	public Holdings editHoldings(@RequestBody Holdings holdings) {
		return service2.saveHoldings(holdings);
	}

	@DeleteMapping(value = "/holdings/{stockTicker}")
	public String deleteStockTicker(@PathVariable String stockTicker) {
		return service2.deleteHoldingsByName(stockTicker);
	}
	
	

}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


