package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PentagonHackathonApplication {

	public static void main(String[] args) {
		SpringApplication.run(PentagonHackathonApplication.class, args);

		// Checking if committed--- Don't mind its useless
	}

}
