
CREATE SCHEMA stocktrades;
CREATE TABLE stocktrades.stock (
  `id` INT NOT NULL auto_increment,
  `stockTicker` VARCHAR(50) NOT NULL,
  `price` DOUBLE NOT NULL,
  `volume` INT NOT NULL,
  `buyOrSell` VARCHAR(25) NOT NULL,
  `statusCode` INT NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE stocktrades.holdings
(
StockTicker varchar(50) not null,
Volume int not null,
Buying_price double not null);