import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";

@Component({
  selector: 'app-trade-history',
  templateUrl: './trade-history.component.html',
  styleUrls: ['./trade-history.component.css']
})
export class TradeHistoryComponent implements OnInit {

  Stocks: any = [];
  constructor( 
    public restApi: RestApiService
    ) { }

  ngOnInit(): void {
    this.loadStocks()
  }

  loadStocks(){
    return this.restApi.getStocks().subscribe((data: {}) => {
      this.Stocks = data;
    })
  }

  deleteStock(id:any){
    if (window.confirm("Are you sure you want to delete the Stock entry?")){
      this.restApi.deleteStock(id).subscribe(data =>{
        this.loadStocks()
      })
    }
  }

}
