import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BuyOrderComponent } from './buy-order/buy-order.component';
import { StockTickerListComponent } from './stock-ticker-list/stock-ticker-list.component';

import { TradeHistoryComponent } from './trade-history/trade-history.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'trade-history' },
  { path: 'trade-history', component: TradeHistoryComponent },

  { path: 'stock-ticker-list', component: StockTickerListComponent },
  { path: 'buy-order', component: BuyOrderComponent }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
