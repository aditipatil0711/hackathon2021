import { Component,Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from "../shared/rest-api.service";

@Component({
  selector: 'app-buy-order',
  templateUrl: './buy-order.component.html',
  styleUrls: ['./buy-order.component.css']
})
export class BuyOrderComponent implements OnInit {
  @Input() stockDetails = { id: 0, stockTicker: '', price: 0.00, volume:0, buyOrSell:"Buy", statusCode: 0 }

  constructor(
    public RestApi: RestApiService,
    public router: Router
  ) { }

  ngOnInit(): void {}

  addStock(){
    this.RestApi.createStock(this.stockDetails).subscribe((data: {}) =>{
      this.router.navigate(['/trade-history'])
    })
  }
}
