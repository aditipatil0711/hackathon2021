import { Component, OnInit } from '@angular/core';
import { DataAccessService } from "../shared/data-access.service";

@Component({
  selector: 'app-stock-ticker-list',
  templateUrl: './stock-ticker-list.component.html',
  styleUrls: ['./stock-ticker-list.component.css']
})
export class StockTickerListComponent implements OnInit {

  Data1: any=[];
  constructor(
    public dataAccess: DataAccessService
    ) { }

  ngOnInit(): void {
    this.loadData()
  }
  loadData(){
    return this.dataAccess.getData().subscribe((data: {}) => {
      this.Data1 = data;
  })
  }
}
